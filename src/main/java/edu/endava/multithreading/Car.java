package edu.endava.multithreading;

public class Car implements Runnable {
    private String name;
    private int time;

    public Car(String name, int time) {
        this.name = name;
        this.time = time;
    }

    @Override
    public void run() {
        enterParking();
        leaveParking();
    }

    private void enterParking() {
        System.out.println(this + " -> parking.");
        try {
            Thread.sleep(this.time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void leaveParking() {
        System.out.println(this + " left parking. " + this.time / 1000 + "s");
    }

    @Override
    public String toString() {
        return "[" + this.name + "]";
    }
}