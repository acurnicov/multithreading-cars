package edu.endava.multithreading;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Application {
    public static void main(String[] args) {
        final int PARKING_CAPACITY = 10;
        ExecutorService parking = Executors.newFixedThreadPool(PARKING_CAPACITY);
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < 20; i++) {
            Runnable car = new Car("Car" + i, random.nextInt(10000));
            parking.submit(car);
        }
        parking.shutdown();

        try {
            parking.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("The day ended.");
    }
}
